import React from 'react';
import {render} from 'react-dom';
import _ from 'underscore'

var ReactHighcharts = require('react-highcharts')

class BarChart extends React.Component {
    componentDidMount() {
        let chart = this.refs.chart.getChart();
        console.log(chart)
    }

    render() {
        let config = {
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: ['Apples', 'Oranges', 'Pears', 'Bananas', 'Plums']
            },
            series: [{
                type: 'column',
                name: 'Jane',
                data: [3, 2, 1, 3, 4]
            }, {
                type: 'column',
                name: 'John',
                data: [2, 3, 5, 7, 6]
            }, {
                type: 'column',
                name: 'Joe',
                data: [4, 3, 3, 9, 0]
            }, {
                type: 'spline',
                name: 'Average',
                data: [3, 2.67, 3, 6.33, 3.33],
                marker: {
                    lineWidth: 2,
                    //lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'white'
                }
            }]
        }
        return <ReactHighcharts config={this.props.config} ref="chart"/>;
    }
}

class App extends React.Component {

    constructor() {
        super()
    }

    render() {
        let data = [
            {country: 'USA', name: 'moe', age: 40, company: "Apple"},
            {country: 'USA', name: 'cheick', age: 28, company: "Apple"},
            {country: 'France', name: 'mimy', age: 28, company: "Nestle"},
            {country: 'France', name: 'ousmane', age: 27, company: "Nestle"},
            {country: 'France', name: 'elay', age: 28, company: "Shinitech"},
            {country: 'USA', name: 'larry', age: 50, company: "Microsoft"},
            {country: 'USA', name: 'curly', age: 60, company: "Google"}
        ]
        let values = {}
        let some = this.groupByProperties(data, 0, values, ['country', 'company'], ['age'])
        console.log("Values", values);
        console.log("Some", some)
        console.log("Initial data", data)
        let series = _.map(_.groupBy(data, 'country'), (value, key) => {
            let res = this.computeChartData(value);
            return {
                type: "column",
                name: key,
                data: res.ages
            }
        })
        console.log(series);
        let chartData = this.computeChartData(data);
        let config = {
            title: {
                text: 'Combination chart'
            },
            xAxis: {
                categories: chartData.categories
            },
            series: series,
            tooltip: {
                valueSuffix: ' ans'
            }
        }
        return <BarChart config={config}/>;
    }

    computeChartData(data) {
        let result = _.groupBy(data, 'company');
        let categories = _.keys(result);
        let companies = _.countBy(data, 'company');
        let count = _.values(companies);
        let ages = _.map(_.values(result), function (elem) {
            let array = _.map(elem, function (item) {
                return item.age
            });
            return _.reduce(array, function (a, b) {
                return a + b
            }, 0) / (array.length > 0 ? array.length : 1)
        })
        console.log(ages);
        return {
            data: result,
            categories: categories,
            companies: companies,
            count: count,
            ages: ages
        }
    }

    /**
     *
     * @param data : array data to group.
     * @param index for recursion
     * @param properties : @param data properties if data has no properties of properties[i] this property is skipped
     * @returns {*} grouped data
     */
    groupByProperties(data, index, values = {}, properties = [], numericProperties = []) {
        if (properties.length === 0 || index >= properties.length) {
            return this.applyStats(data, numericProperties)
        }
        if (_.has(data[0], properties[index]) === false) {
            return this.groupByProperties(data, index + 1, values, properties, numericProperties)
        }
        let grouped = _.groupBy(data, properties[index])
        let result = {};
        values[properties[index]] = values[properties[index]] || []
        values[properties[index]] = values[properties[index]].concat(_.keys(grouped))
        for (let item in grouped) {
            result[item] = this.groupByProperties(grouped[item], index + 1, values, properties, numericProperties)
        }
        return result
    }

    /**
     * 
     * @param array
     * @param numericProperties be careful
     * @returns {*}
     */
    applyStats(array, numericProperties = []) {
        if (numericProperties.length === 0)
            return {count: array.length}
        let result = {count: 0, average: {}}
        for (let prop of numericProperties) { // Initialisation
            result[prop] = 0
            result['average'][prop] = 0
        }
        let i = 1
        for (let item of array) {
            result['count'] = i
            for (let prop of numericProperties) { // Initialisation
                if(_.has(item, prop)) {
                    result[prop] += item[prop]
                    result['average'][prop] *= (i - 1)
                    result['average'][prop] += item[prop]
                    result['average'][prop] /= i
                }
            }
            i++
        }
        return result
    }
}

render(<App/>, document.getElementById('root'));